# SystemD container service

How to create a systemd service to control a container start and stop 

```bash
sudo nano /etc/systemd/system/docker-containername.service
```

```
[Unit]
Description=Container name description
After=docker.service
Requires=docker.service
 
[Service]
Restart=always
ExecStart=/usr/bin/docker start -a containername
ExecStop=/usr/bin/docker stop -t 2 containername
 
[Install]
WantedBy=multi-user.target
```


```bash
sudo systemctl daemon-reload
sudo systemctl start docker-containername
sudo systemctl enable docker-containername
```